package wgus

import (
	"errors"
)

const (
	ProtocolVersion         = `1.11`
	MetadataProtocolVersion = `7.5`
	InstallationID          = `go-wgus`
)

var ErrUnknownClientType = errors.New(`unknown client_type, try to check metadata`)
